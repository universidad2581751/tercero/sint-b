#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Modo de uso: ./corrector.sh <cuenta>"
    exit 1
fi

cuenta=$1
errores=0

postUrl=(
    'libros'                # 1 Devuelve todos los libros
    'libros/autor/4'        # 2 Devuelve todos los libros de un autor que si existe
    'libro/671'             # 3 Devuelve un libro que si existe
    'paises'                # 4 Devuelve todos los paises
    'pais/T8'               # 5 Devuelve un pais que si existe
    'autores'               # 6 Devuelve todos los autores
    'autor/4'               # 7 Devuelve un autor que si existe
    'autores/pais/T8'       # 8 Devuelve todos los autores de un pais que si existe
    'autores/pais/45'       # 9 Devuelve todos los autores de un pais que no existe
    'libros/autor/4L'       # 10 Devuelve todos los libros de un autor que no existe
)

return_codes=(200 200 200 200 200 200 200 200 404 404)

puerto=$((7000 + cuenta))
preURL="http://localhost:$puerto/sint$cuenta/P2Lib/v1/"

declare -a url

# Componer las llamadas a la API
for ((i=0; i<${#postUrl[@]}; i++)); do
    url+=("$preURL${postUrl[$i]}")
    # echo "${url[$i]}"
done

for ((i=0; i<${#url[@]}; i++)); do
    response_raw=$(curl -s "${url[$i]}")  # Obtener la respuesta
    response=$(echo -n "$response_raw" | sed -e 's/[[:space:]]//g') 
    result_code=$(curl -s -o /dev/null -w "%{http_code}" "${url[$i]}")

    expected_result_url="http://luis.sabucedo.webs.uvigo.es/responses/response$((i+1))" 
    expected_result_raw=$(curl -s "$expected_result_url")   
    expected_result=$(echo -n "$expected_result_raw" | sed -e 's/[[:space:]]//g')


    echo -n "Test $i. " 
    #echo "Obtiene"
    #echo "$response"
    #echo "Debería obtener"
    #echo "$expected_result"  

    echo "$expected_result" | jq --sort-keys .  > temp1.json
    echo "$response" | jq --sort-keys .  > temp2.json
     
    if (diff -b temp1.json temp2.json)  &&  ( test "$result_code" = "${return_codes[$i]}" ); then
        echo "Correcto"
    else
        echo "Fallido"
        ((errores++))
    fi
     echo "----------------------"

done

if [ "$errores" -eq "0" ]; then
    echo "Evaluación inicial correcta"
    echo "Autorizado para realizar la entrega"
else
    echo "Evaluación inicial fallida"
    echo "Errores: $errores"
fi

